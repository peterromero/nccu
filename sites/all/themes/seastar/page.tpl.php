<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 * least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 * or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 * when linking to the front page. This includes the language domain or
 * prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 * in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 * in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 * site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 * the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 * modules, intended to be displayed in front of the main title tag that
 * appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 * modules, intended to be displayed after the main title tag that appears in
 * the template.
 * - $messages: HTML for status and error messages. Should be displayed
 * prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 * (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 * menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 * associated with the page, and the node ID is the second argument
 * in the page's path (e.g. node/12345 and node/12345/revisions, but not
 * comment/reply/12345).
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<header class="main">
	<div class="mountains"></div>
	<div class="width_limiter">
		<div class="block_container allow_overflow">
			<h1 class="nccu_grid_block col_6">
				<a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">Northern Colorado Credit Union</a>
			</h1>
			<section class="tethered_login nccu_grid_block col_6">
        <div class="lock nccu_grid_block col_3 of_6">
          <p class="title">Log in to Home Banking &nbsp;
            <a href="/home-banking-help" class="login_help">Login Help</a>
          </p>
        </div>
        <div class="nccu_grid_block col_3 of_6 social_media_icons">
          <?php echo($social_media_icons); ?>
        </div>
        <?php echo($tethered_login_form_desktop); ?>
        <?php echo($login_button_mobile); ?>
      </section>
      <section class="nccu_grid_block col_6">
        <div class="nccu_grid_block menus col_6 of_6">
          <?php echo($masthead_menu); ?>
        </div>
      </section>
      <nav class="main nccu_grid_block full"><?php print render($page['nav_bar']); ?></nav>
		</div>
	</div>
	<div class="curve top"></div>
</header>
<?php if ($page['carousel']): ?>
	<section class="carousel">
		<?php print render($page['carousel']); ?>
		<div class="curve bottom"></div>
	</section>
	<div class="carousel_spacer"><div class="spinner"></div></div>
<?php endif; ?>
<section class="content">
	<div class="width_limiter">
		<div class="block_container">
			<?php if ($page['sidebar_right']): ?>
				<div class="main_content nccu_grid_block col_9">
			<?php else: ?>
				<div class="main_content nccu_grid_block col_12">
			<?php endif; ?>
				<?php if ($breadcrumb): ?>
					<?php print $breadcrumb; ?>
				<?php endif; ?>
				<?php if (!$is_front): ?>
					<?php print render($title_prefix); ?>
					<?php if ($title): ?><h2 class="title"><?php print $title; ?></h2><?php endif; ?>
					<?php print render($title_suffix); ?>
					<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
				<?php endif; ?>
				<?php print $messages; ?>
				<?php print render($page['help']); ?>
				<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
				<?php print render($page['content']); ?>
			</div>
			<?php if ($page['sidebar_right']): ?>
				<div class="sidebar_right nccu_grid_block col_3">
					<?php print render($page['sidebar_right']); ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($page['feature1'] || $page['feature2']): ?>
			<div class="block_container features">
				<?php if ($page['feature1']): ?>
					<div class="nccu_grid_block col_6 feature">
						<?php print render($page['feature1']); ?>
					</div>
				<?php endif; ?>
				<?php if ($page['feature2']): ?>
				<div class="nccu_grid_block col_6 feature">
					<?php print render($page['feature2']); ?>
				</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</section>
<footer>
	<div class="width_limiter">
		<div class="block_container">
			<div class="nccu_grid_block col_8">
				<?php if ($page['footer_left']): ?>
					<?php print render($page['footer_left']); ?>
				<?php else: ?>
					&nbsp;
				<?php endif; ?>
			</div>
			<div class="nccu_grid_block col_4">
				<?php print render($page['footer_right']); ?>
			</div>
		</div>
	</div>
</footer>
