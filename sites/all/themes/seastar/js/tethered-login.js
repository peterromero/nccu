(function (UsernameWidget, $) {

  UsernameWidget.CheckEnterKey = function (e) {
    if (e.keyCode === 13 || e.which === 13) {
      e.preventDefault();
      $("#LoginButton").click();
    }
  };

  $(document).ready(function () {

    $("#UsernameTextBox").keypress(function (e) {
      UsernameWidget.CheckEnterKey(e);
    });

    $("#UsernameTextBox").focus();
  });

})(window.UsernameWidget = window.UsernameWidget || {}, jQuery);
