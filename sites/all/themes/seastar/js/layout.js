jQuery(function() {
	jQuery('select').selectBoxIt({autoWidth: false});
});
jQuery(window).load(function() {
	snap_footer_to_bottom();
	equalize_feature_heights();
	spread_out_main_nav_links();
	activate_dropdown_menus({delay: 400});
	activate_search_form();
	jQuery('nav.main').css('opacity', 1);
	jQuery('#fake_login').click(function() {
		alert('Online Home Banking is currently unavailable. Please reload this page to try again. If the problem persists, please contact Northern Colorado Credit Union at 970.330.3900.');
	});
});
jQuery(window).resize(function() {
	snap_footer_to_bottom();
	spread_out_main_nav_links();

	equalize_feature_heights();
	activate_dropdown_menus({delay: 400});
	// activate_search_form();
});
function equalize_feature_heights() {
	if (get_css_width() > 768) {
		if (jQuery('.feature').length > 1) {
			var tallest_feature_height = 0;
			jQuery.each(jQuery('.feature'), function() {
				if (jQuery(this).height() > tallest_feature_height) tallest_feature_height = jQuery(this).height();
			});
			jQuery.each(jQuery('.feature'), function() {
				jQuery(this).height(tallest_feature_height);
			});
		}
	}
}
function snap_footer_to_bottom() {
	if (get_css_width() > 768) {
		var window_height = jQuery(window).height();
		var footer_height_with_margin = jQuery('footer').outerHeight(true);
		var body_height = jQuery('body').outerHeight(true);
		var content_height;
		var footer_mode = jQuery('footer').css('position');
		switch(footer_mode) {
			case 'relative': {
				content_height = body_height;
				if (content_height < window_height) {
					jQuery('footer').css('position', 'absolute').css('top', (window_height - footer_height_with_margin));
				}
				break;
			}
			case 'absolute': {
				content_height = body_height + footer_height_with_margin;
				if (content_height < window_height) {
					jQuery('footer').css('top', (window_height - footer_height_with_margin));
				} else {
					jQuery('footer').css('position', 'relative').css('top', '');
				}
				break;
			}
		}
	}
}
function spread_out_main_nav_links() {
	var top_level_links = jQuery('nav.main .menu-block-wrapper > .menu > li > a, nav.main .menu-block-wrapper > .menu > li > span.nolink');
	if (get_css_width() > 768) {
		var nav_width = jQuery('nav.main').innerWidth();
		var links_width = 0;
		jQuery.each(top_level_links, function(i, link) {
			links_width += jQuery(link).outerWidth();
		});
		var gutter = (nav_width - links_width - 1) / (top_level_links.length - 1);
		jQuery.each(top_level_links, function(i, link) {
			if (i < top_level_links.length - 1) {
				jQuery(link).css('margin-right', gutter + 'px');
			}
		});
	} else {
		top_level_links.css('margin-right', 0);
	}
}
function activate_dropdown_menus(args) {
	// Remove existing events that may have been attached at a different screen resolution
	jQuery('nav.main .menu-block-wrapper > .menu > li.expanded > a').off('mouseover mouseout click');
	jQuery('nav.main .menu .menu a').off('mouseover mouseout');
	if (get_css_width() > 768) {
		// Attach new events
		jQuery('nav.main .menu-block-wrapper > .menu > li.expanded > a').mouseover(function() {
			var li = jQuery(this).parents('li');
			_show_dropdown_menu(li);
		}).mouseout(function() {
			var li = jQuery(this).parents('li');
			_hide_dropdown_menu(li, args.delay);
		});
		jQuery('nav.main .menu .menu a').mouseover(function(e) {
			e.stopPropagation();
			var li = jQuery(this).parents('li.expanded');
			_show_dropdown_menu(li);
		}).mouseout(function(e) {
			e.stopPropagation();
			var li = jQuery(this).parents('li.expanded');
			_hide_dropdown_menu(li, args.delay);
		});
	} else {
		// Attach new events
		jQuery('nav.main .menu-block-wrapper > .menu > li.expanded > a').click(function(e) {
			e.preventDefault();
			var li = jQuery(this).parents('li');
			_toggle_dropdown_menu(li);
		});
	}
}
function _toggle_dropdown_menu(parent_li) {
	if (parent_li.hasClass('open')) {
		_hide_dropdown_menu(parent_li);
	} else {
		_show_dropdown_menu(parent_li);
	}
}
function _show_dropdown_menu(parent_li) {
	if (get_css_width() > 768) {
		clearTimeout(parent_li.data('menu_close_delay'));
		var menu = parent_li.find('.menu');
		menu.css('left', 'auto');
		parent_li.addClass('open');
		_hide_search_form();
	} else {
		parent_li.addClass('open');
	}
}
function _hide_dropdown_menu(parent_li, delay) {
	if (get_css_width() > 768) {
		var menu = parent_li.find('.menu');
		parent_li.data('menu_close_delay', setTimeout(function() {
			parent_li.removeClass('open');
			setTimeout(function() {
				menu.css('left', '-10000px');
			}, 300);
		}, delay));
	} else {
		parent_li.removeClass('open');
	}
}
function activate_search_form() {
	jQuery('nav .menu .search').click(function(e) {
		e.stopPropagation();
		_toggle_search_form();
	});
	jQuery('#block-search-form').click(function(e) {
		e.stopPropagation();
	});
	jQuery('body').click(function() {
		_hide_search_form();
	});
}
function _toggle_search_form() {
	var search_form = jQuery('#block-search-form');
	if (search_form.hasClass('open')) {
		_hide_search_form();
	} else {
		_show_search_form();
	}
}
function _show_search_form() {
	if (get_css_width() > 768) {
		var search_button = jQuery('nav .menu .search');
		var search_form = jQuery('#block-search-form');
		search_button.addClass('open');
		search_form.css('left', 'auto').css('right', '0').addClass('open');
		jQuery('#block-search-form .form-item input[type=text]').focus();
	} else {
		var search_form = jQuery('#block-search-form');
		search_form.addClass('open');
		jQuery('#block-search-form .form-item input[type=text]').focus();
	}
}
function _hide_search_form() {
	if (get_css_width() > 768) {
		var search_button = jQuery('nav .menu .search');
		var search_form = jQuery('#block-search-form');
		search_button.removeClass('open');
		search_form.removeClass('open');
		setTimeout(function() {
			search_form.css('left', '-10000px').css('right', 'auto');
		}, 300);
	} else {
		var search_form = jQuery('#block-search-form');
		search_form.removeClass('open');
	}
}
function get_css_width() {
	return window.innerWidth;
}
