<?php

/**
 * Implements hook_block_info().
 */
function nccu_rate_loader_block_info() {
  $blocks = array(
    'cl_mortgages_home' => array(
      'info' => 'CL Mortgage Rates (Home Page)'
    ),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function nccu_rate_loader_block_view($delta = '') {
  $output = array(
    'subject' => '',
    'content' => _nccu_rate_loader_content($delta)
  );

  return $output;
}

function _nccu_rate_loader_content($delta) {
  $output = '';
  switch ($delta) {
    case 'cl_mortgages_home':
      $rates = array();

      $rates[30] = _nccu_rate_loader_get_rate('nccu_mortgage_rates', '30 Year Fixed Rate');
      $rates[15] = _nccu_rate_loader_get_rate('nccu_mortgage_rates', '15 Year Fixed Rate');

      $rows = array();

      foreach ($rates as $term => $apr) {
        if ($apr == FALSE) {
          $rate_str = 'Could not load today\'s rate';
        }
        else {
          $rate_str = 'as low as <span class="rate">' . $apr . '%</span><span class="rate_type">APR*</span>';
        }
        $rows[] = array(
          'data' => array(
            l(t($term . '-Year Fixed'), '/rates'),
            $rate_str
          ),
          'no_striping' => TRUE
        );
      }

      $output = array(
        'container' => array(
          '#type' => 'container',
          '#attributes' => array(
            'id' => 'centennial-rates',
            'class' => array(
              'view-home-page-rates'
            )
          ),
          'title' => array(
            '#type' => 'markup',
            '#markup' => '<h3>Mortgages</h3>'
          ),
          'rate_table' => array(
            '#theme' => 'table',
            '#rows' => $rows
          )
        )
      );
      break;
  }

  return $output;
}

/**
 * @param string $rate_set The set of rates to draw from. Only allowed value is
 * currently 'nccu_mortgage_rates'.
 * @param string $label The rate to load from Centennial Mortgage's page
 * (https://www.centennial-lending.com/dailyrates/), identified by the text in
 * the Product cell of the row in question.
 * @return mixed The requested rate as a float, or FALSE if the value could not
 * be loaded.
 */
function _nccu_rate_loader_get_rate($rate_set, $label) {
  // Get the most recently loaded rates from the cache.
  $rates = cache_get($rate_set, 'cache');
  if (!$rates) {
    // If there are no rates in the cache, rebuild the cache.
    _nccu_rate_loader_refresh_rates($rate_set);
    $rates = cache_get($rate_set, 'cache');
    if (!$rates) {
      // If that still didn't work, something is wrong. Check the logs.
      return FALSE;
    }
  }
  if (array_key_exists($label, $rates->data)) {
    return $rates->data[$label];
  }
  else {
    watchdog('nccu_rate_loader', t('Unknown rate label requested: @l'), array('@l' => $label), WATCHDOG_WARNING);
    return FALSE;
  }
}

/**
 * Loads the entire rate page from Centennial Mortgage, parses it, and caches
 * the values.
 * @param string $rate_set The set of rates to get. Only allowed value is
 * currently 'nccu_mortgage_rates'.
 * @return boolean TRUE on success, FALSE on failure.
 */
function _nccu_rate_loader_refresh_rates($rate_set) {
  switch ($rate_set) {
    case 'nccu_mortgage_rates':
      // Load the page contents from Centennial Mortgage.
      $curl_connection = curl_init();

      curl_setopt($curl_connection, CURLOPT_URL, 'https://www.centennial-lending.com/residential-mortgages/');
      curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, FALSE);

      $remote_page_content = curl_exec($curl_connection);
      $curl_error = curl_error($curl_connection);

      curl_close($curl_connection);

      if ($curl_error) {
        watchdog('nccu_rate_loader', t('cURL error: @e'), array('@e' => $curl_error), WATCHDOG_ERROR);
        return FALSE;
      }

      $rates = array();

      // Parse the HTML.
      $html = new DOMDocument();
      if (@$html->loadHTML($remote_page_content)) {
        // Get all divs on the page.
        $divs = $html->getElementsByTagName('div');
        if ($divs->length > 0) {
          /** @var \DOMElement $div */
          foreach ($divs as $div) {
            $classes = explode(' ', $div->getAttribute('class'));
            // Look for a div with the class fusion-text.
            if (in_array('fusion-text', $classes)) {
              // Within the div we're looking for, the first child is a <p>
              // containing the rate information.
              if ($first_child = $div->firstChild) {
                if ($first_child->nodeType == 1 && $first_child->tagName == 'p') {
                  $matches = array();
                  $matches_found = preg_match("/Today's Rate: +([\d\.]+)%/", $first_child->textContent, $matches);
                  if ($matches_found) {
                    $rates[] = $matches[1];
                  }
                }
              }
            }
          }
        }
      }
      else {
        watchdog('nccu_rate_loader', t('Could not parse HTML into DOMDocument. HTML: @h'), array('@h' => $remote_page_content), WATCHDOG_ERROR);
        return FALSE;
      }

      $rates['30 Year Fixed Rate'] = $rates[0];
      $rates['15 Year Fixed Rate'] = $rates[1];
      unset($rates[0]);
      unset($rates[1]);

      cache_set($rate_set, $rates, 'cache', CACHE_TEMPORARY);

      return TRUE;
      break;
    default:
      watchdog('nccu_rate_loader', t('Unrecognized rate set requested (@r).'), array('@r' => $rate_set), WATCHDOG_WARNING);
      return FALSE;
      break;
  }
}

/**
 * Implements hook_token_info().
 */
function nccu_rate_loader_token_info() {
  $output = array(
    'types' => array(
      'rate' => array(
        'name' => t('NCCU rate loader'),
        'description' => t('Rates provided by the NCCU Rate Loader module.')
      )
    ),
    'tokens' => array(
      'rate' => array(
        'conv_15_fix' => array(
          'name' => t('Conventional 15 Year Fixed Rate'),
          'description' => t('Conventional 15 Year Fixed Rate')
        ),
        'conv_30_fix' => array(
          'name' => t('Conventional 30 Year Fixed Rate'),
          'description' => t('Conventional 30 Year Fixed Rate')
        ),
      )
    )
  );

  return $output;
}

/**
 * Implements hook_tokens().
 */
function nccu_rate_loader_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  switch ($type) {
    case 'rate':
      foreach ($tokens as $name => $original_value) {
        switch ($name) {
          case 'conv_15_fix':
            $replacements[$original_value] = _nccu_rate_loader_get_rate('nccu_mortgage_rates', '15 Year Fixed Rate');
            break;
          case 'conv_30_fix':
            $replacements[$original_value] = _nccu_rate_loader_get_rate('nccu_mortgage_rates', '30 Year Fixed Rate');
            break;
        }
      }
      break;
  }
  return $replacements;
}
