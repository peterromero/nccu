function onVmeReady() {
  var params = {
    niptf: 'Issuer_FDNBDemo',
    nicaller: 'US_FDNBDemo',
    nicmp: 'FDNBDemoBrandedEnroll',
    niflow: 'DEMO',
    nichn: 'BrandedEnroll',
    niseg: '00001',
    locale: 'en_US'
  };
  V.init({params: params});
}
