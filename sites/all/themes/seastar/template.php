<?php
function seastar_preprocess_html(&$vars) {
  $nccu_timezone = new DateTimeZone('America/Denver');
  $nccu_now = new DateTime('now', $nccu_timezone);
  $nccu_time_offset_hours = $nccu_timezone->getOffset($nccu_now) / 3600;
  $nccu_sunrise = date_sunrise(time(), SUNFUNCS_RET_TIMESTAMP, 40.415405, -104.693084, 90.58333, $nccu_time_offset_hours);
  $nccu_sunset = date_sunset(time(), SUNFUNCS_RET_TIMESTAMP, 40.415405, -104.693084, 90.58333, $nccu_time_offset_hours);
  $nccu_time_of_day = 'day';
  if ($nccu_now->getTimestamp() > $nccu_sunrise && $nccu_now->getTimestamp() < $nccu_sunset) {
    $nccu_time_of_day = 'day';
  }
  $vars['classes_array'][] = $nccu_time_of_day;

  drupal_add_js(drupal_get_path('theme', 'seastar') . '/js/anticlickjack.js');
  drupal_add_js(drupal_get_path('theme', 'seastar') . '/js/tethered-login.js');

  switch (current_path()) {
    case 'node/158': // nccu-visa-platinum-rewards-credit-card
      drupal_add_js('https://i.simpli.fi/dpx.js?cid=40696&action=100&segment=northerncoloradocreditunion&m=1&sifi_tuid=20067');
      break;
    case 'node/211': // visa-checkout
      drupal_add_js(drupal_get_path('theme', 'seastar') . '/js/visa-checkout-enrollment.js');
      drupal_add_js('https://sandbox.secure.checkout.visa.com/assets/integration/v1/sdk.js');
      break;
  }
}

function seastar_preprocess_page(&$vars) {
  $vars['masthead_menu'] = theme('links', array(
    'links' => menu_navigation_links('menu-masthead-links'),
    'attributes' => array(
      'class' => 'links masthead_menu'
    )
  ));
  $vars['social_media_icons'] = theme('links', array(
    'links' => menu_navigation_links('menu-social-media-icons'),
    'attributes' => array(
      'class' => 'links'
    )
  ));

  // Tethered login form
  $vars['tethered_login_form_desktop'] = <<<HTML
<a href="https://obc.itsme247.com/939/" class="tmp_link" target="_blank">Click here to access the Online Banking Login</a>
HTML;

  $vars['login_button_mobile'] = '<a class="button_mobile" href="https://obc.itsme247.com/939">Online Banking Login</a>';
}

function seastar_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode(' &nbsp;&nbsp;>&nbsp;&nbsp; ', $breadcrumb) . '</div>';
    return $output;
  }
}

function seastar_preprocess_image(&$vars) {
  foreach (array('width', 'height') as $key) {
    // Removes inline width and height on images.
    unset($vars[$key]);
  }
}

function seastar_preprocess_search_result(&$vars) {
  unset($vars['info_split']['user']);
  $vars['info'] = implode(' - ', $vars['info_split']);
}

function seastar_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['placeholder'] = t('Enter search terms…');
  }
}

function seastar_query_alter(&$query) {
  $is_search = FALSE;
  foreach ($query->getTables() as $table) {
    if ($table['table'] == 'search_index') {
      $is_search = TRUE;
    }
  }
  if ($is_search) {
    $query->condition('n.type', 'carousel_slide', '<>');
    $query->condition('n.type', 'footer_logo_link', '<>');
    $query->condition('n.type', 'rate', '<>');
    $query->condition('n.type', 'special_notice', '<>');
  }
}
